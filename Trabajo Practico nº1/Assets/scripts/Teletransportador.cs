using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransportador : MonoBehaviour
{
    public Transform Target;
    public GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        player.transform.position = Target.transform.position;
    }


}
