using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorController : MonoBehaviour
{
    public Transform topFloor;
    public Transform bottomFloor;
    public float speed = 1f;
    public float floorHeight = 2f;

    private bool goingUp = true;
    private Vector3 targetPosition;

    void Start()
    {
        // Set the initial target position to the top floor
        targetPosition = topFloor.position;
    }

    void FixedUpdate()
    {
        // Move the elevator towards the target position
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        // Check if the elevator has reached the target position
        if (transform.position == targetPosition)
        {
            // If the elevator has reached the top floor, go down
            if (goingUp && transform.position == topFloor.position)
            {
                targetPosition = bottomFloor.position;
                goingUp = false;
            }
            // If the elevator has reached the bottom floor, go up
            else if (!goingUp && transform.position == bottomFloor.position)
            {
                targetPosition = topFloor.position;
                goingUp = true;
            }
        }
    }

    //void OnCollisionEnter(Collision collision)
   // {
        // Stop the elevator if it collides with something
      //  speed = 0f;
    //}

    void OnCollisionExit(Collision collision)
    {
        // Resume moving the elevator after the collision is over
        speed = 1f;
    }
}


