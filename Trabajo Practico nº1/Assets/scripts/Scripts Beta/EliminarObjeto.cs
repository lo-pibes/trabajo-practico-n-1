using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EliminarObjeto : MonoBehaviour
{
    public GameObject objetoADesactivar;

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("boton"))
        {
            objetoADesactivar.SetActive(false);
        }
    }
}