using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugadores : MonoBehaviour
{
    
    public GameObject jugador1;
    public GameObject jugador2;

    
    public float velocidad;

   
    private GameObject jugadorActual;

    void Start()
    {
        
        jugadorActual = jugador1;
    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (jugadorActual == jugador1)
            {
                jugadorActual = jugador2;
            }
            else
            {
                jugadorActual = jugador1;
            }
        }

        
        float h = Input.GetAxis("Horizontal");
        

        
        Vector3 movimiento = new Vector3(h, 0f) * velocidad * Time.deltaTime;

       
        jugadorActual.transform.Translate(movimiento);
    }
}