using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EliminarObjeto2 : MonoBehaviour
{
    public GameObject objetoADesactivar;

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("boton 2"))
        {
            objetoADesactivar.SetActive(false);
        }
    }
}
