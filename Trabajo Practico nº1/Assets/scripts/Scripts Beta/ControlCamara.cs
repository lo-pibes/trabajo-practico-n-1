using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    // Referencias a los objetos de los personajes y sus respectivas c�maras
    public GameObject personaje1;
    public GameObject personaje2;
    public Camera camaraPersonaje1;
    public Camera camaraPersonaje2;

    void Start()
    {
        // Desactivar la c�mara del segundo personaje al inicio del juego
        camaraPersonaje2.enabled = false;
    }

    void Update()
    {
        // Si se presiona un bot�n para cambiar de c�mara, activar/desactivar las c�maras correspondientes
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            // Si la c�mara del primer personaje est� activa, desactivarla y activar la c�mara del segundo personaje
            if (camaraPersonaje1.enabled)
            {
                camaraPersonaje1.enabled = false;
                camaraPersonaje2.enabled = true;
            }
            // Si la c�mara del segundo personaje est� activa, desactivarla y activar la c�mara del primer personaje
            else
            {
                camaraPersonaje1.enabled = true;
                camaraPersonaje2.enabled = false;
            }
        }
    }
}