using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public float velocidad = 0f;

    public float fuerzaSalto = 7f; 
    public bool enSuelo = true;
    public int saltosMaximos = 2; 
    public int saltosRestantes;

    void Start()
    {
        saltosRestantes = saltosMaximos; // inicializar la cantidad de saltos restantes al m�ximo
    }

    void Update()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        transform.Translate(new Vector3(movimientoHorizontal * velocidad * Time.deltaTime, 0, 0));

        if (enSuelo) // si el personaje est� en el suelo
        {
            saltosRestantes = saltosMaximos; // resetear la cantidad de saltos restantes
        }

        if (saltosRestantes > 0 && Input.GetKeyDown(KeyCode.Space)) // si quedan saltos restantes y se presiona la tecla de espacio
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse); // aplicar una fuerza de salto hacia arriba
            enSuelo = false; // indicar que el personaje no est� en el suelo
            saltosRestantes--; // restar un salto
        }

    }
    void OnCollisionEnter(Collision colision)
    {
        if (colision.gameObject.tag == "Suelo") // si el personaje colisiona con un objeto con el tag "Suelo"
        {
            enSuelo = true; // indicar que el personaje est� en el suelo
            saltosRestantes = saltosMaximos; // resetear la cantidad de saltos restantes
        }
    }
}


